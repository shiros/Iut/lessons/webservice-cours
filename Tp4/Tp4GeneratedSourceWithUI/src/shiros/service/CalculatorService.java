

/**
 * CalculatorService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.8  Built on : May 19, 2018 (07:06:11 BST)
 */

    package shiros.service;

    /*
     *  CalculatorService java interface
     */

    public interface CalculatorService {
          

        /**
          * Auto generated method signature
          * 
                    * @param calcul0                
             * @throws shiros.service.CalculatorServiceCalculatorService_ShirOSExceptionException : 
         */

         
                     public shiros.service.CalculResponse calcul(

                        shiros.service.Calcul calcul0)
                        throws java.rmi.RemoteException
             
          ,shiros.service.CalculatorServiceCalculatorService_ShirOSExceptionException;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param calcul0           
          */
        public void startcalcul(

            shiros.service.Calcul calcul0,

            final shiros.service.CalculatorServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param version2            
         */

         
                     public shiros.service.VersionResponse version(

                        shiros.service.Version version2)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param version2           
          */
        public void startversion(

            shiros.service.Version version2,

            final shiros.service.CalculatorServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        
       //
       }
    