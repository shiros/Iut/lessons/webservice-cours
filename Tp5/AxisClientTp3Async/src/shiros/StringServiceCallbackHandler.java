/**
 * StringServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.2  Built on : May 02, 2016 (05:55:18 BST)
 */
package shiros;


/**
 *  StringServiceCallbackHandler Callback class, Users can extend this class and implement
 *  their own receiveResult and receiveError methods.
 */
public abstract class StringServiceCallbackHandler {
    protected Object clientData;

    /**
     * User can pass in any object that needs to be accessed once the NonBlocking
     * Web service call is finished and appropriate method of this CallBack is called.
     * @param clientData Object mechanism by which the user can pass in user data
     * that will be avilable at the time this callback is called.
     */
    public StringServiceCallbackHandler(Object clientData) {
        this.clientData = clientData;
    }

    /**
     * Please use this constructor if you don't want to set any clientData
     */
    public StringServiceCallbackHandler() {
        this.clientData = null;
    }

    /**
     * Get the client data
     */
    public Object getClientData() {
        return clientData;
    }

    /**
     * auto generated Axis2 call back method for readStr method
     * override this method for handling normal response from readStr operation
     */
    public void receiveResultreadStr(
        shiros.StringServiceStub.ReadStrResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from readStr operation
     */
    public void receiveErrorreadStr(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for concat method
     * override this method for handling normal response from concat operation
     */
    public void receiveResultconcat(
        shiros.StringServiceStub.ConcatResponse result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from concat operation
     */
    public void receiveErrorconcat(java.lang.Exception e) {
    }
}
