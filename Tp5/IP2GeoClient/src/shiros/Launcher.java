package shiros;

public class Launcher {
    public static void main(String args[]) {
        try {
            IP2GeoStub stub = new IP2GeoStub();
            
            // Call Resolve Ip method
            IP2GeoStub.ResolveIP resolveIP = new IP2GeoStub.ResolveIP();
            //resolveIP.setIpAddress("172.217.21.78");
            resolveIP.setIpAddress("51.254.34.15");
            resolveIP.setLicenseKey("0");
            IP2GeoStub.ResolveIPResponse response = stub.resolveIP(resolveIP);

            IP2GeoStub.IPInformation ipInformation = response.getResolveIPResult();
            
            System.out.println(String.format("City : %s", ipInformation.getCity()));
            System.out.println(String.format("State Province : %s", ipInformation.getStateProvince()));
            System.out.println(String.format("Country : %s", ipInformation.getCountry()));
            System.out.println(String.format("Country Code : %s", ipInformation.getCountryCode()));
            System.out.println(String.format("Organization : %s", ipInformation.getOrganization()));
            System.out.println(String.format("Latitude : %s", ipInformation.getLatitude()));
            System.out.println(String.format("Longitude : %s", ipInformation.getLongitude()));
            System.out.println(String.format("Area Code : %s", ipInformation.getAreaCode()));
            System.out.println(String.format("Time Zone : %s", ipInformation.getTimeZone()));
        } catch (Exception exception) {
            exception.printStackTrace();
            System.out.println("\n\n\n");
        }
    }
}