/**
 * VersionExceptionException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.2  Built on : May 02, 2016 (05:55:18 BST)
 */
package shiros;

public class VersionExceptionException extends java.lang.Exception {
    private static final long serialVersionUID = 1528113196221L;
    private shiros.VersionStub.VersionException faultMessage;

    public VersionExceptionException() {
        super("VersionExceptionException");
    }

    public VersionExceptionException(java.lang.String s) {
        super(s);
    }

    public VersionExceptionException(java.lang.String s, java.lang.Throwable ex) {
        super(s, ex);
    }

    public VersionExceptionException(java.lang.Throwable cause) {
        super(cause);
    }

    public void setFaultMessage(shiros.VersionStub.VersionException msg) {
        faultMessage = msg;
    }

    public shiros.VersionStub.VersionException getFaultMessage() {
        return faultMessage;
    }
}
