/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shiros.service;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.servlet.http.HttpSession;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import shiros.exception.ShirOSException;

/**
 *
 * @author alcaillot1
 */
@WebService(serviceName = "HomeService")
public class HomeService {
    //Accessing HTTP Session on the Server
    @Resource    
    private WebServiceContext wsContext;    
    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "convertNumberToString")
    public String convertNumberToString(@WebParam(name = "number") int number) {
        return Integer.toString(number);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "division")
    public int division(@WebParam(name = "number1") int number1, @WebParam(name = "number2") int number2) throws ShirOSException {
        try {            
            return number1 / number2;
        } catch (ArithmeticException arithmeticException) {
            throw new ShirOSException("The 2nd parameter can't be 0. The division operation can't be process with zero on denominator", arithmeticException);
        } catch (Throwable throwable) {
            throw new ShirOSException("Error during the proccessing of request", throwable);                
        }
    }

    /**
     * Web service operation
     */
    @Resource
    @WebMethod(operationName = "divisionWithSession")
    public int divisionWithSession(@WebParam(name = "number1") int number1, @WebParam(name = "number2") int number2) throws ShirOSException {
        MessageContext mc = wsContext.getMessageContext();    
        HttpSession session = ((javax.servlet.http.HttpServletRequest)mc.get(MessageContext.SERVLET_REQUEST)).getSession();
        
        if (session != null) {
            throw new ShirOSException("Session can't be null");
        }
        
        try {
            Integer result = (Integer)session.getAttribute("Result");
            
            if (result == null) {
                result = number1 / number2;
            }
            
            session.setAttribute("Result", result);
            
            return result;
        } catch (ArithmeticException arithmeticException) {
            throw new ShirOSException("The 2nd parameter can't be 0. The division operation can't be process with zero on denominator", arithmeticException);
        } catch (Throwable throwable) {
            throw new ShirOSException("Error during the proccessing of request", throwable);                
        }
    }
}
